
# Pynab webui dockerfiles

I wanted to dockerize the pynab webui, and I also found that building it inside docker was easier than settings up the node/grunt/bower stuff on my desktop.  Copy the files in this repo into the webui/ folder of pynab, then run the suggested commands, or modify them to suit your needs.

## Build the build image

`docker build -t "pynab/build_webui" -f Dockerfile.build .`

## Run the build image with a volume to copy the webui into

If you're on a mac, and using boot2docker, it will have the /Users directory shared, but you need to use an absolute path

`docker run -v /Users/bettse/Projects/pynab/webui/dist:/webui/dist -it pynab/build_webui /bin/bash -c 'grunt build --force'`

## Build the run image

`docker build -t "pynab/webui" -f Dockerfile.run .`

## Run the run image

I choose 8111 arbitrarily

`docker run -d -p 8111:80 pynab/webui`

